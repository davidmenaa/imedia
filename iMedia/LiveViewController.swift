//
//  LiveViewController.swift
//  iMedia
//
//  Created by David Mena on 1/12/16.
//  Copyright © 2016 David Mena. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class LiveViewController: UITableViewController {
    
    var liveList: [UAMedio]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        liveList = UAFuenteDatos.datos.listadirecto
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (liveList?.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LiveCell", for: indexPath) as! UACell
        
        let medio:UAMedio =  liveList![indexPath.row]
        
        cell.title.text = medio.titulo
        cell.imageCell.image = medio.portada
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let medio = liveList![indexPath.row]
        
        let controller = AVPlayerViewController()
        
        let player = AVPlayer(url: medio.url as URL)
        
        controller.player = player
        
        self.present(controller, animated: true, completion: {_ in })
    }

}
