
import UIKit

class UACell : UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var imageCell: UIImageView!
}
