//
//  UAFuenteDatos.swift
//  iMedia
//
//  Created by David Mena on 1/12/16.
//  Copyright © 2016 David Mena. All rights reserved.
//

import Foundation
import UIKit

class UAFuenteDatos {
    
    var listaradio = [UAMedio]()
    var listavideo = [UAMedio]()
    var listadirecto = [UAMedio]()
    
    private init() {
        
        //Radio Array
        var song = UAMedio(titulo:"Birthday Suit",
                           artista:"Don Cavalli",
                           album:"Temperamental",
                           url:Bundle.main.url(forResource: "Don_Cavalli_-_Birthday_Suit", withExtension:"mp3")!)
        
        song.portada = UIImage(named: "Don_Cavalli")
        self.listaradio.append(song)
        
        song = UAMedio(titulo:"Beach, Beach, Beach!",
                       artista:"Bosco Rogers",
                       album:"Post Exotic",
                       url:Bundle.main.url(forResource: "Bosco_Rogers_-_Beach_Beach_Beach", withExtension:"mp3")!)
        
        song.portada = UIImage(named: "Bosco_Rogers")
        
        self.listaradio.append(song)
    
        
        song = UAMedio(titulo:"Roady",
                               artista:"Fat Freddy's Drop",
                               album:"Based on a True Story",
                               url:Bundle.main.url(forResource: "Fat_Freddys_Drop_-_Roady", withExtension:"mp3")!)
        
        song.portada = UIImage(named: "Fat_Freddys")
        
        self.listaradio.append(song)
        
    
        song = UAMedio(titulo:"Punchin' Bag",
                       artista:"Cage The Elephant",
                       album:"Tell Me I'm Pretty",
                       url:Bundle.main.url(forResource: "Cage_The_Elephant_-_Punchin_Bag", withExtension:"mp3")!)

        song.portada = UIImage(named: "Cage_The_Elephant")
        
        self.listaradio.append(song)
        
        // --------------------------------------------------------------------
        
        //VOD Array
        
        var video = UAMedio(titulo:"Mad Max - Fury Road",
                            artista:"Warner Bros Pictures",
                            album:"Best Trailers",
                            url: URL(string: "http://192.168.1.11:1935/vod/mp4:mad_max-fury_road.mp4/playlist.m3u8")!)
        
        video.portada = UIImage(named: "mad_max-fury_road.jpg")
        self.listavideo.append(video)
        
        video = UAMedio(titulo:"Spiderman - Homecoming",
                        artista:"Sony Pictures",
                        album:"Best Trailers",
                        url: URL(string: "http://192.168.1.11:1935/vod/mp4:spiderman-homecoming.mp4/playlist.m3u8")!)
        
        video.portada = UIImage(named: "spiderman-homecoming.jpg")
        self.listavideo.append(video)
        
        video = UAMedio(titulo:"Transformers 5 - The Last Knight",
                        artista:"Paramount Pictures",
                        album:"Best Trailers",
                        url: URL(string: "http://192.168.1.11:1935/vod/mp4:transformers-the_last_knight.mp4/playlist.m3u8")!)
        
        video.portada = UIImage(named: "transformers-the_last_knight.jpg")
        self.listavideo.append(video)
        
        video = UAMedio(titulo:"Star Wars - Rogue One",
                        artista:"LucasFilms Ltd.",
                        album:"Best Trailers",
                        url: URL(string: "http://192.168.1.11:1935/vod/mp4:star_wars-rogue_one.mp4/playlist.m3u8")!)
        
        video.portada = UIImage(named: "star_wars-rogue_one.jpg")
        self.listavideo.append(video)

        
        // --------------------------------------------------------------------
        //Live Array

        var onair = UAMedio(titulo:"Channel 1",
                            artista:"Android live user",
                            album:"MyStreaming",
                            url: URL(string: "http://192.168.1.11:1935/live/canal1/playlist.m3u8")!)
        
        onair.portada = UIImage(named: "live_cover.png")
        self.listadirecto.append(onair)
        
        onair = UAMedio(titulo:"Channel 2",
                        artista:"Android live user",
                        album:"MyStreaming",
                        url: URL(string: "http://192.168.1.11:1935/live/canal2/playlist.m3u8")!)
        
        onair.portada = UIImage(named: "live_cover.png")
        self.listadirecto.append(onair)
        
        onair = UAMedio(titulo:"Channel 3",
                        artista:"Android live user",
                        album:"MyStreaming",
                        url: URL(string: "http://192.168.1.11:1935/live/canal3/playlist.m3u8")!)
        
        onair.portada = UIImage(named: "live_cover.png")
        self.listadirecto.append(onair)
        
    }
    
    public static var sharedInstance : UAFuenteDatos {
        get { return datos }
    }
    
    static let datos : UAFuenteDatos = UAFuenteDatos()
    
}
