//
//  RadioViewController.swift
//  iMedia
//
//  Created by David Mena on 1/12/16.
//  Copyright © 2016 David Mena. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer

class RadioViewController: UITableViewController {
    
    var radioList: [UAMedio]?
    
    var player: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        radioList = UAFuenteDatos.datos.listaradio
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        self.becomeFirstResponder()
        
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            try audioSession.setActive(true)
        }
        catch _ {
        }
        
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (radioList?.count)!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RadioCell", for: indexPath) as! UACell

        let medio:UAMedio =  radioList![indexPath.row]
        
        cell.title?.text = medio.artista
        cell.desc?.text = medio.titulo
        cell.imageCell.image = medio.portada
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let medio:UAMedio =  radioList![indexPath.row]
        
        player = try! AVAudioPlayer(contentsOf: medio.url)
        player?.prepareToPlay()
        player?.play()
    
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle: medio.titulo,
                                                           MPMediaItemPropertyArtist: medio.artista,
                                                           MPMediaItemPropertyAlbumTitle: medio.album]
    }

    override func remoteControlReceived(with event: UIEvent?) {
        if event?.type == .remoteControl {
            if event?.subtype == .remoteControlTogglePlayPause {
                if (self.player?.isPlaying)! {
                    self.player?.pause()
                }
                else {
                    self.player?.play()
                }
            }
            else if event?.subtype == .remoteControlPause {
                self.player?.pause()
            }
            else if event?.subtype == .remoteControlPlay {
                self.player?.play()
            }
        }
    }
    
}
