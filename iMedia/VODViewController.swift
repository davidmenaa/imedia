//
//  VODViewController.swift
//  iMedia
//
//  Created by David Mena on 1/12/16.
//  Copyright © 2016 David Mena. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class VODViewController: UITableViewController {
    
    var vodList: [UAMedio]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        vodList = UAFuenteDatos.datos.listavideo
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (vodList?.count)!
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VODCell", for: indexPath) as! UACell
        
        let medio:UAMedio =  vodList![indexPath.row]

        cell.title.text = medio.titulo
        cell.imageCell.image = medio.portada
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let medio = vodList![indexPath.row]
    
        let controller = AVPlayerViewController()
        
        let player = AVPlayer(url: medio.url as URL)
    
        controller.player = player
        
        self.present(controller, animated: true, completion: {_ in })
    }
}
