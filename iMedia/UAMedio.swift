//
//  UAMedio.swift
//  iMedia
//
//  Created by David Mena on 1/12/16.
//  Copyright © 2016 David Mena. All rights reserved.
//

import Foundation
import UIKit

class UAMedio {
    var titulo : String
    var artista : String
    var album : String
    var portada : UIImage?
    var url : URL
    
    
    init(titulo: String, artista: String, album: String, url:URL) {
        self.titulo = titulo
        self.artista = artista
        self.album = album
        self.url = url
    }
    
//    init(titulo: String, artista: String, album: String, portada: UIImage, url: URL) {
//        self.titulo = titulo
//        self.artista = artista
//        self.album = album
//        self.portada = portada
//        self.url = url
//    }
}
