//
//  FirstViewController.swift
//  iMedia
//
//  Created by David Mena on 1/12/16.
//  Copyright © 2016 David Mena. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var grafica: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Creamos vista y añadimos a  la vista actual
        let pieChartView = PieChartView()
        pieChartView.frame = CGRect(x: 0, y: 20, width: grafica.frame.width, height: grafica.frame.height)
        pieChartView.segments = [
            Segment(color: UIColor.red, value: CGFloat(UAFuenteDatos.sharedInstance.listadirecto.count), texto: "Vídeo en directo"),
            Segment(color: UIColor.blue, value: CGFloat(UAFuenteDatos.sharedInstance.listavideo.count), texto: "Vídeo bajo demanda" ),
            Segment(color: UIColor.yellow, value: CGFloat(UAFuenteDatos.sharedInstance.listaradio.count), texto: "Podcasts")
        ]
        pieChartView.titulo = ""
        grafica.addSubview(pieChartView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

